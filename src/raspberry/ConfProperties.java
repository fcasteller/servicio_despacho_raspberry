/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicionotificaciones;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fernando
 */
public class ConfProperties {

    private Properties properties;
    private String server_smtp;
    private String send_by_database;
    private String email_from;
    private String email_to;
    private Integer tiempo_detenido;
    private Integer tiempo_dentro_geocerca;
    private String usuario;
    private FileInputStream file;

    public ConfProperties() {
        Properties properties = new Properties();
        this.properties = properties;
        try {
            file = new FileInputStream ("./properties/conf.properties");
            this.properties.load(file);
//            this.properties.load(new FileInputStream("/servicio_notificaciones/properties/conf.properties"));

        } catch (IOException ex) {
            Logger.getLogger(ConfProperties.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public String getServer_smtp() {
        this.server_smtp = this.properties.getProperty("server_smtp");
        return server_smtp;
    }

    public String getSend_by_database() {
        this.send_by_database = this.properties.getProperty("send_by_database");
        return send_by_database;
    }

    public boolean getBooleanSendToDataBase() {
        if ("true".equals(this.properties.getProperty("send_by_database"))) {
            return true;
        } else {
            return false;
        }
    }

    public String getEmail_from() {
        this.email_from = this.properties.getProperty("email_from");
        return email_from;
    }

    public String getEmail_to() {
        this.email_to = this.properties.getProperty("email_to");
        return email_to;
    }

    public Integer getTiempo_detenido_gc() {
        this.tiempo_detenido = Integer.parseInt(this.properties.getProperty("tiempo_detenido_godoy_cruz"));
        return tiempo_detenido;
    }

    public Integer getTiempo_detenido_lh() {
        this.tiempo_detenido = Integer.parseInt(this.properties.getProperty("tiempo_detenido_las_heras"));
        return tiempo_detenido;
    }

    public Integer getTiempo_Dentro_Geocerca_GC() {
        this.tiempo_dentro_geocerca = Integer.parseInt(this.properties.getProperty("tiempo_dentro_geocerca_oeste_godoy_cruz"));
        return tiempo_dentro_geocerca;
    }

    public String getUsuarioGC() {
        this.usuario = this.properties.getProperty("usuario_godoy_cruz");
        return usuario;
    }

    public String getUsuarioLH() {
        this.usuario = this.properties.getProperty("usuario_las_heras");
        return usuario;
    }

    public String getUsuarioOGC() {
        this.usuario = this.properties.getProperty("usuario_oeste_godoy_cruz");
        return usuario;
    }

    public void setServer_smtp(String server_smtp) {
        this.server_smtp = server_smtp;
    }

    public void setSend_by_database(String send_by_database) throws FileNotFoundException, IOException {
        OutputStream output = null;
        output = new FileOutputStream("/properties/conf.properties");

        this.properties.setProperty("send_by_database", send_by_database);
        this.properties.store(output, null);
        this.send_by_database = send_by_database;
    }

    public void setEmail_from(String email_from) {
        this.email_from = email_from;
    }

    public void setEmail_to(String email_to) {
        this.email_to = email_to;
    }

    public void setTiempo_detenido_gc(Integer tiempo_detenido) {
        this.tiempo_detenido = tiempo_detenido;
    }

    public void setTiempo_detenido_lh(Integer tiempo_detenido) {
        this.tiempo_detenido = tiempo_detenido;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

}
