/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package raspberry;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fernando
 */
public class Listener {

    private final String OK = "OK";
    final GpioController gpioled;
    final GpioController gpiopulsador;

    public Listener() {
        gpioled = GpioFactory.getInstance();
        gpiopulsador = GpioFactory.getInstance();
    }

    public void shutdownControllerGpioled() {
        System.out.println("apagando controlador led");
        gpioled.shutdown();
    }

    public void shutdownControllerGpiopulsador() {
        System.out.println("apagando controlador pulsador");
        gpiopulsador.shutdown();
    }

    public void start() throws InterruptedException {
        System.out.println("Iniciando Listener GPIO ...");
        // create gpio controller PULSADOR

        // provision gpio pin #03 as an input pin with its internal pull down resistor enabled
        final GpioPinDigitalInput pulsador = gpiopulsador.provisionDigitalInputPin(RaspiPin.GPIO_03, PinPullResistance.PULL_DOWN);
        // PARA LED
        final GpioPinDigitalOutput led = gpioled.provisionDigitalOutputPin(RaspiPin.GPIO_00, "MyLED", PinState.LOW);
        //estado del led
        led.setShutdownOptions(true, PinState.LOW);

        System.out.println("Listener GPIO iniciado ...");
        // create and register gpio pin listener
        pulsador.addListener(new GpioPinListenerDigital() {
            private long lasthigh;
            private long disablehigh = 10000;

            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event
            ) {
//                System.out.println("GPIO PIN STATE CHANGE: " + event.getPin() + " = " + event.getState());
                if (event.getState() == PinState.LOW) {
                    //System.out.println("LOW");
                } else {
                    //System.out.println("HIGH");
                    long now = (new Date()).getTime();
                    if (now < lasthigh + disablehigh) {
                        float diferencia = now - (lasthigh + disablehigh);
                        float difsec = (diferencia / 1000) * -1;
                        System.out.println("espere, faltan " + String.format("%.1f", difsec) + " segundos");
                    } else {
                        lasthigh = now;
                        led.pulse(disablehigh);
                        try {
                            Process process = Runtime.getRuntime().exec("bash /home/pi/script.sh");
                            InputStream inputstream = process.getInputStream();
                            InputStreamReader isr = new InputStreamReader(inputstream);
                            BufferedReader br = new BufferedReader(isr);

                            String line;
                            String resultado = null;
                            while ((line = br.readLine()) != null) {
                                resultado = line;
                            }
                            if (resultado.equals(OK)) {
                                System.out.println("POST: " + resultado);
                                System.out.println("Reproduciendo Sonido");
                                Process sonido = Runtime.getRuntime().exec("sudo aplay -vv /home/pi/Music/viaje_despachado_ximena.wav");
                            } else {
                                System.out.println("NO SE ENVIO");
                                Process sonido_no_disponible = Runtime.getRuntime().exec("sudo aplay -vv /home/pi/Music/sistema_no_disponible_ximena.wav");

                            }
                        } catch (IOException ex) {
                            System.out.println("NO SE PUDO EJECUTAR EL COMANDO");
                            Logger.getLogger(Listener.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        }
        );

        // EXIT: (CTRL-C)
        for (;;) {
            Thread.sleep(500);
        }
    }
}
