/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package raspberry;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.commons.daemon.*;

/**
 *
 * @author fernando
 */
public class Demonio implements Daemon {

    private Listener listen = new Listener();

    public static void main(String[] args) {
    }

    @Override
    public void init(DaemonContext dc) throws DaemonInitException, Exception {
        System.out.println("Iniciando Daemon...");
    }

    @Override
    public void start() throws Exception {
        System.out.println("Iniciando Servicio Despacho Raspberry...");
        main(null);
        listen.start();
    }

    @Override
    public void stop() throws Exception {
        System.out.println("Deteniendo Servicio Despacho Raspberry ...");
        listen.shutdownControllerGpioled();
        listen.shutdownControllerGpiopulsador();
    }

    @Override
    public void destroy() {
        System.out.println("Servicio detenido.");
    }
}
